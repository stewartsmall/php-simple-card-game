<?php
	
	// Developed by Stewart Small
    // September 29th, 2017
    // License: https://creativecommons.org/licenses/by/4.0/legalcode

    interface IPlayer {
        public function SetUserName($name);
        public function PlayCard($index);
        public function ReplaceACard($card);
        public function SetHand($hand);
        public function GetHand();
    }

?>