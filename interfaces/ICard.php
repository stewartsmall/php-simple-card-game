<?php

	// Developed by Stewart Small
    // September 29th, 2017
    // License: https://creativecommons.org/licenses/by/4.0/legalcode
 
	interface ICard
	{
		public function DisplayCard($index);
		public function RemoveCard();
		public function InsertCard($card);	
		public function GetCard($index);
		public function SetCards($cards);	
		public function GetAllCards();
	}
 
?>