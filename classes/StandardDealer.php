<?php

    // Developed by Stewart Small
    // September 29th, 2017
    // License: https://creativecommons.org/licenses/by/4.0/legalcode

    include './interfaces/IDealer.php';

    class StandardDealer implements IDealer {
        public function Shuffle($deck) {
            $cards = $deck->GetAllCards();
            shuffle($cards);
            return $cards;
        }

        public function Deal($deck, $limit) {
            $hand = [];
            $shuffled = $this->Shuffle($deck);
            $deck->SetCards($shuffled);
            for($i = 0; $i < $limit; $i++){
                array_push($hand, $deck->GetCard($i));
                $deck->RemoveCard();
            }

            return $hand;
        }

        public function ReStackDeck($deck, $cards){
            for($j = 0; $j < count($cards); $j++) {
                $deck->InsertCard($cards[$j]);
            }
        }
    }
    
?>