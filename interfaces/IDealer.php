<?php

	// Developed by Stewart Small
    // September 29th, 2017
    // License: https://creativecommons.org/licenses/by/4.0/legalcode

    interface IDealer {
        public function Deal($deck, $limit);
        public function Shuffle($deck);
        public function ReStackDeck($deck, $cards);
    }
?>