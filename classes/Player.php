<?php

    // Developed by Stewart Small
    // September 29th, 2017
    // License: https://creativecommons.org/licenses/by/4.0/legalcode

    include './interfaces/IPlayer.php';

    class Player implements IPlayer {
        private $userName = "";
        private $hand = [];

        public function SetHand($hand) {
            $this->hand = $hand;
        }

        public function GetHand() {
            return $this->hand;
        }

        public function PlayCard($index) {
            $card = $this->hand[$index];
            array_splice($this->hand, $index, 1);
            return $card;
        }

        public function ReplaceACard($card) {
            array_push($this->hand, $card);
        }

        public function SetUserName($name) {
            $this->username = $name;
        }

        public function GetUserName(){
            return $this->username;
        }
    }

?>