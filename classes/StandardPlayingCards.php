<?php

    // Developed by Stewart Small
    // September 29th, 2017
    // License: https://creativecommons.org/licenses/by/4.0/legalcode

    include './interfaces/ICard.php';

    class StandardPlayingCards implements ICard {
        private $suites = ['hearts', 'clubs', 'diamonds', 'spades'];
        private $values = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King', 'Ace'];
        private $colors = ['red', 'black'];
        private $cards = [];

        public function __construct() {
            for($i = 0; $i < count($this->suites); $i++){
                $color = ($i%2 === 0)? $this->colors[0] : $this->colors[1];
                for($j = 0; $j < count($this->values); $j++){
                    $this->cards[] = $this->suites[$i].":".$this->values[$j].":".$color;
                }
            }
        }

        public function DisplayCard($card) {
            $card = explode(":", $card);
            return $card[1]." of ".$card[0];
        }

        public function RemoveCard(){
            array_shift($this->cards);
        }

        public function InsertCard($card) {
            array_push($this->cards, $card);
        }

        public function GetCard($index) {
            return $this->cards[$index];
        }

        public function GetAllCards(){
            return $this->cards;
        }

        public function SetCards($cards) {
            $this->cards = $cards;
        }

    }

?>