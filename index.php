<?php

    // Developed by Stewart Small
    // September 29th, 2017
    // License: https://creativecommons.org/licenses/by/4.0/legalcode

    include './classes/StandardPlayingCards.php';
    include './classes/StandardDealer.php';
    include './classes/Player.php';

    $deck = new StandardPlayingCards();
    $dealer = new StandardDealer();
    $player1 = new Player();
    $player2 = new Player();

    // Initialise the card pile
    $pile = [];

    // Deal 7 cards to player1 and player2
    $player1->SetHand($dealer->Deal($deck, 7));
    $player2->SetHand($dealer->Deal($deck, 7));

     // Show contents of players hands
    echo "<pre>";
    var_dump($player1->GetHand());
    var_dump($player2->GetHand());
    echo "</pre>";

    // play cards from players hand at specified indexes
    array_push($pile, $player1->PlayCard(3));
    array_push($pile, $player2->PlayCard(5));

    // Show the card pile
    echo "<pre>";
    var_dump($pile);

    // Show contents of players hands
    var_dump($player1->GetHand());
    var_dump($player2->GetHand());
    echo "</pre>";

    // Dealer deals a card to player 1
    $pickCard = $dealer->Deal($deck, 1);
    $player1->ReplaceACard($pickCard[0]);

    echo "<pre>";
    var_dump($player1->GetHand());
    echo "</pre>";
?>